# trasm

A virtual machine for an imaginary architecture that doesn't follow classical rules. Just a fun project to see how far I can bend the rules.

[Link to assembler](https://gitlab.com/AidoP1/trasm)


# Instructions

Format

|   Byte   | Usage                                  |
| :------: | :------------------------------------- |
|    1     | The instruction code                   |
|    2     | Flags specifying if address or pointer |
| 3, 5 & 7 | First byte of operand 1, 2 & 3         |
| 4, 6 & 8 | Last byte of operand 1, 2 & 3          |

If address or pointer simply use the operand bytes as a number.

First Byte:
* If the MSB is set, use output register

Last  Byte:
* Stores the data of a constant
