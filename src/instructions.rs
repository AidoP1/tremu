// Implements the instruction set

use crate::{Bus, Register, Registers};

pub enum Fault {
    None,
    Stop,
    Explicit,
    DivideByZero,
    InvalidAddress,
    InvalidAccess,
    InvalidMemory,
    InvalidInstruction,
    InvalidOperand
}

pub fn execute(instruction: (u8, u8, Register, Register, Register), bus: &mut Bus, mut registers: &mut Registers) -> Fault {
    let op_left = instruction.2;
    let op_right = instruction.3;
    let op_other = instruction.4;
    let flags = instruction.1;
    let instruction = instruction.0;

    match instruction {
        0x30 => {       // JMP. Unconditional jump to address
            if let Register::address(address) = op_left {
                registers.program_counter = address;
                Fault::None
            } else { Fault::InvalidOperand }
        },
        0x31 => {       // JMZ. Conditional jump to address if given byte is zero
            if let Register::address(address) = op_left {
                if (bus.unwrap_register(op_right, &registers) == 0) { registers.program_counter = address };
                Fault::None
            } else { Fault::InvalidOperand }
        },
        0x32 => {       // JME. Conditional jump to address if given bytes equate
            if let Register::address(address) = op_left {
                if bus.unwrap_register(op_right, &registers) == bus.unwrap_register(op_other, &registers) { registers.program_counter = address };
                Fault::None
            } else { Fault::InvalidOperand }
        },
        0x33 => {       // JNE. Conditional jump to address if given bytes do not equate
            if let Register::address(address) = op_left {
                if bus.unwrap_register(op_right, &registers) != bus.unwrap_register(op_other, &registers) { registers.program_counter = address };
                Fault::None
            } else { Fault::InvalidOperand }
        },
        0x40 => {       // SET. Assign data on the address bus
            if let Register::address(address) = op_left {
                //println!("Set {:?} to {:?}", address, &op_right);
                bus.write_byte(address, bus.unwrap_register(op_right, &registers));
                Fault::None
            } else { Fault::InvalidOperand }
        },
        0x41 => {       // PTR. Assign 16 bytes to memory (for pointers)
            if let Register::address(address) = op_left {
                bus.write_byte(address, bus.unwrap_register(op_right, &registers));
                bus.write_byte(address + 1, bus.unwrap_register(op_other, &registers));
                Fault::None
            } else { Fault::InvalidOperand }
        },
        0xA0 => {       // ADD. Add two bytes, placing result in other
            let result = bus.unwrap_register(op_left, registers) + bus.unwrap_register(op_right, registers);
            bus.assign_register(op_other, &mut registers, result);
            Fault::None
        },
        0xA1 => {       // SUB. Subtract two bytes
            let result = bus.unwrap_register(op_left, registers) - bus.unwrap_register(op_right, registers);
            bus.assign_register(op_other, &mut registers, result);
            Fault::None
        },
        0xA2 => {       // MUL. Multiply two bytes
            let result = bus.unwrap_register(op_left, registers) * bus.unwrap_register(op_right, registers);
            bus.assign_register(op_other, &mut registers, result);
            Fault::None
        },
        0xA3 => {       // DIV. Divide two bytes
            let right = bus.unwrap_register(op_right, registers);
            if right == 0 { return Fault::DivideByZero };
            let result = bus.unwrap_register(op_left, registers) / right;
            bus.assign_register(op_other, &mut registers, result);
            Fault::None
        },
        0xA4 => {       // MOD. Modulo two bytes
            let result = bus.unwrap_register(op_left, registers) % bus.unwrap_register(op_right, registers);
            bus.assign_register(op_other, &mut registers, result);
            Fault::None
        },
        0xEA => {       // STP. Stops all execution and flushes IO
            Fault::Stop
        },
        0xEE => {       // NOP. No Operations. Useful for keeping operands, basically an instruction comment
            Fault::None
        },
        0xEF => {       // FAULT. Forces a fault
            Fault::Explicit
        },
        _ => return Fault::InvalidInstruction
    }
}