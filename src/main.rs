// Tremu is a simple PC emulator for an imaginary instruction set

#![feature(exclusive_range_pattern)]

use std::{ io, io::{ Write, Read } };

mod instructions;
use instructions::Fault;

// Clock Speed
const CLOCK: usize      = 1024;                         // 1024 clocks per second
const OP_SIZE: u16      = 8;                            // 8 Bytes per operation

pub mod addresses {
    pub mod size {
        pub const MEMORY: u16       = 32 * 1024;        // 32K memory
        pub const ROM: u16          = 1024;             // 1K ROM
        pub const FAULT: u16        = crate::OP_SIZE;   // Stores a fault instruction to prevent executing past ROM
        pub const VIDEO_BUFFER: u16 = 64 * 64;          // 64 x 64 screen or terminal
        pub const STDIN: u16        = 1;                // 1B pipe to stdin (updates with every clock)
        pub const STDOUT: u16       = 1;                // 1B pipe to stdout (writes out every clock)
        pub const STDERR: u16       = 1;                // 1B pipe to stderr (writes out every clock)
        pub const FILE: u16         = 8 * 1024;         // 8K File on host system
    }

    pub mod location {
        use crate::addresses;
        pub const MEMORY: u16       =                 0;
        pub const ROM: u16          =                 addresses::size::MEMORY;
        pub const FAULT: u16        = ROM           + addresses::size::ROM;
        pub const VIDEO_BUFFER: u16 = FAULT         + addresses::size::FAULT;
        pub const STDIN: u16        = VIDEO_BUFFER  + addresses::size::VIDEO_BUFFER;
        pub const STDOUT: u16       = STDIN         + addresses::size::STDIN;
        pub const STDERR: u16       = STDOUT        + addresses::size::STDOUT;
        pub const FILE: u16         = STDERR        + addresses::size::STDERR;
        pub const END: u16          = FILE          + addresses::size::FILE;
    }
}

pub struct Bus {
    memory:         [u8; addresses::size::MEMORY        as usize],
    pub rom:        [u8; addresses::size::ROM           as usize],
    fault:          [u8; addresses::size::FAULT         as usize],
    video_buffer:   [u8; addresses::size::VIDEO_BUFFER  as usize],
    file:           [u8; addresses::size::FILE          as usize]
}

impl Bus {
    pub fn new() -> Self {
        Self {
            memory:         [0; addresses::size::MEMORY         as usize],
            rom:            [0; addresses::size::ROM            as usize],
            fault:          [0; addresses::size::FAULT          as usize],
            video_buffer:   [0; addresses::size::VIDEO_BUFFER   as usize],
            file:           [0; addresses::size::FILE           as usize]
        }
    }

    pub fn read_byte(&self, address: u16) -> u8 {
        if address      < addresses::location::ROM          { self.memory[      address                                         as usize] }
        else if address < addresses::location::FAULT        { self.rom[         (address - addresses::location::ROM)            as usize] }
        else if address < addresses::location::VIDEO_BUFFER { self.fault[       (address - addresses::location::FAULT)          as usize] }
        else if address < addresses::location::STDIN        { self.video_buffer[(address - addresses::location::VIDEO_BUFFER)   as usize] }
        else if address < addresses::location::STDOUT       {
            let mut data = [0; 1];
            if let Ok(size) = io::stdin().read(&mut data) {
                match size {
                    1 => data[0],
                    _ => 0
                }
            } else {
                panic!("Error with stdin")
            }
        }
        else if address < addresses::location::STDERR       { 0 }
        else if address < addresses::location::FILE         { 0 }
        else if address < addresses::location::END          { self.file[(address - addresses::location::FILE) as usize] }
        else { 0 }
    }

    pub fn write_byte(&mut self, address: u16, data: u8) -> Fault {
        if address      < addresses::location::ROM          { self.memory[address as usize] = data; Fault::None }
        else if address < addresses::location::FAULT        { Fault::InvalidAccess }
        else if address < addresses::location::VIDEO_BUFFER { Fault::InvalidAccess }
        else if address < addresses::location::STDIN        { self.video_buffer[(address - addresses::location::VIDEO_BUFFER) as usize] = data; Fault::None }
        else if address < addresses::location::STDOUT       { Fault::InvalidAccess }
        else if address < addresses::location::STDERR       { let data = [data]; io::stdout().write(&data).expect("Failed to write to stdout"); io::stdout().flush(); Fault::None }
        else if address < addresses::location::FILE         { let data = [data]; io::stderr().write(&data).expect("Failed to write to stderr"); io::stderr().flush(); Fault::None }
        else if address < addresses::location::END          { self.file[(address - addresses::location::FILE) as usize] = data; Fault::None }
        else { Fault::InvalidAddress }
    }

    // Read an instruction from the specified address
    pub fn read_instruction(&self, address: u16) -> (u8, u8, Register, Register, Register) {
        let flags = self.read_byte(address + 1);
        (
            self.read_byte(address),
            flags,
            Register::from_bytes(self.read_byte(address + 2), self.read_byte(address + 3), flags & 0b10000000u8 != 0, flags & 0b00010000u8 != 0),
            Register::from_bytes(self.read_byte(address + 4), self.read_byte(address + 5), flags & 0b01000000u8 != 0, flags & 0b00001000u8 != 0),
            Register::from_bytes(self.read_byte(address + 6), self.read_byte(address + 7), flags & 0b00100000u8 != 0, flags & 0b00000100u8 != 0),

        )
    }

    pub fn unwrap_register(&self, register: Register, registers: &Registers) -> u8 {
        match register {
            Register::data(data) => data,
            Register::address(address) => self.read_byte(address),
            Register::pointer(address) => self.read_byte((self.read_byte(address) as u16) << 8 | self.read_byte(address + 1) as u16),
            Register::output() => registers.output
        }
    }

    pub fn peek_register(&self, register: &Register, registers: &Registers) -> u8 {
        match register {
            Register::data(data) => *data,
            Register::address(address) => self.read_byte(*address),
            Register::pointer(address) => self.read_byte((self.read_byte(*address) as u16) << 8 | self.read_byte(address + 1) as u16),
            Register::output() => registers.output
        }
    }
    
    pub fn assign_register(&mut self, register: Register, registers: &mut Registers, data: u8) -> Fault {
        match register {
            Register::data(data) => Fault::InvalidAccess,
            Register::address(address) => self.write_byte(address, data),
            Register::pointer(address) => self.write_byte((self.read_byte(address) as u16) << 8 | self.read_byte(address + 1) as u16, data),
            Register::output() => { registers.output = data; Fault::None }
        }
    }
}

#[derive(Debug)]
pub enum Register {
    data(u8),       // 2nd byte determines if a register in instruction set
    address(u16),
    output(),
    pointer(u16),
}

impl Register {
    pub fn from_bytes(MSB: u8, LSB: u8, is_address: bool, is_pointer: bool) -> Self {
        if is_address {
            Register::address(((MSB as u16) << 8) | LSB as u16)
        } else if is_pointer {
            Register::pointer(((MSB as u16) << 8) | LSB as u16)
        } else {
            if LSB & 0b10000000u8 != 0 {
                Register::output()
            } else {
                Register::data(MSB)
            }
        }
    }
}

// Registers
pub struct Registers {
    program_counter: u16,
    exec_fault: u16,
    exec_double_fault: u16,
    pub ra: Register,
    pub rb: Register,
    output: u8,
    output_overflow: u8,

    // FLAGS: FAULT_S : DOUBLE_FAULT_S : OVERFLOW
    flags: u8
}

impl Registers {
    pub fn new() -> Self {
        Self {
            program_counter: addresses::location::ROM,
            exec_fault: addresses::location::FAULT,
            exec_double_fault: addresses::location::FAULT,

            ra: Register::data(0),
            rb: Register::data(0),
            output: 0,
            output_overflow: 0,

            flags: 0,
        }
    }

    pub fn clock(&mut self) {
        if self.program_counter >= addresses::location::END - OP_SIZE { return self.fault() }
        self.program_counter += OP_SIZE;
    }

    pub fn fault(&mut self) {
        if self.flags & 0b01000000u8 != 0 {
            // A triple fault occured, kill the system
            println!();
            panic!("System triple fault")
        } if self.flags & 0b10000000u8 != 0 {
            // A double fault occured
            self.program_counter = self.exec_double_fault;
            self.flags |= 0b01000000u8;
        } else {
            // A fault occured
            self.program_counter = self.exec_fault;
            self.flags |= 0b10000000u8;
        }
    }
}

use std::fs::File;

fn main() {
    let mut bus = Bus::new();
    let mut registers = Registers::new();

    // Load the ROM
    let mut rom_file = File::open("program.bin").expect("No ROM to boot processor with");
    match rom_file.read(&mut bus.rom) {
        Ok(n) => println!("Initialised {}B ROM\n", n),
        Err(error) => panic!("Failed to load ROM - {}", error),
    };

    //io::stdout().write(b"\x02").expect("Failed to clear stdout buffer");

    let mut instruction;
    while { instruction = bus.read_instruction(registers.program_counter); true } {
        // Update program counter
        registers.clock();
        
        //println!("Instruction:\t{:X}:                {:?}={}                {:?}={}                {:?}={}", instruction.0, instruction.2, bus.peek_register(&instruction.2, &registers), &instruction.3, bus.peek_register(&instruction.3, &registers),  &instruction.4, bus.peek_register(&instruction.4, &registers));


        // Load and execute instruction
        match instructions::execute(instruction, &mut bus, &mut registers) {
            Fault::None => {  },
            Fault::Stop => { println!("\nExit Requested"); return },
            _=> { registers.fault() },
        }
    }
}
